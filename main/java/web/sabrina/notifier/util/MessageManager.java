package web.sabrina.controller.util;

import android.telephony.gsm.*;

public class MessageManager {
    
    public static void send(String message) {
    	// the phone number is hardcoded for now
    	// and I'm NOT sharing it!
        send("--CENSORED--", message);
    }
    
    public static void send(String to, String message) {
        SmsManager sm = SmsManager.getDefault();
        sm.sendTextMessage(to, null, message, null, null);
    }
    
    public static void schedule() {
        // TODO
    }
}
