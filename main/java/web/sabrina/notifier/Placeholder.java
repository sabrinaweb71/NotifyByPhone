package web.sabrina.notifier;

import android.app.*;
import android.os.*;

public class Placeholder extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.placeholder);
    }
}
