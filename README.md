# NotifyByPhone

This is an app I'm developing just for myself, because I need it.
Each time my tablet (and my future smartphone) receives a notification, it texts me.
This is necessary to me, because I have vision issues (ie, in some lighting conditions, I can't see a thing in a touch screen).